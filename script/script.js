'use strict';

/*First input form*/
const firstInput = document.getElementById('firstIconChange');
firstInput.addEventListener('click', firstIconEvent);

function firstIconEvent() {
    let passwordData = document.getElementById('firstInputPassword');

    if (passwordData.type === 'password') {
        firstInput.classList.remove('fa-eye');
        firstInput.classList.add('fa-eye-slash');
        passwordData.type = 'text';
    } else {
        firstInput.classList.remove('fa-eye-slash');
        firstInput.classList.add('fa-eye');
        passwordData.type = 'password';
    }
}

/*Second input form*/
const secondInput = document.getElementById('secondIconChange');
secondInput.addEventListener('click', secondIconEvent);

function secondIconEvent() {
    let passwordData = document.getElementById('secondInputPassword');

    if (passwordData.type === 'password') {
        secondInput.classList.remove('fa-eye');
        secondInput.classList.add('fa-eye-slash');
        passwordData.type = 'text';
    } else {
        secondInput.classList.remove('fa-eye-slash');
        secondInput.classList.add('fa-eye');
        passwordData.type = 'password';
    }
}

/*Confirm button*/
const confirmButton = document.getElementById('confirmButton');
confirmButton.addEventListener('click', confirmButtonAction);

function confirmButtonAction(e) {
    let firstPassword = document.getElementById('firstInputPassword');
    let secondPassword = document.getElementById('secondInputPassword');

    if (firstPassword.value === secondPassword.value) {
        alert('You are welcome');
    } else {
        e.preventDefault(); //no reload page on button click
        let ifNotTheSame = document.querySelector('.passwordAreNotTheSAme');
        ifNotTheSame.innerText = 'Потрібно ввести однакові значення';
    }
}
